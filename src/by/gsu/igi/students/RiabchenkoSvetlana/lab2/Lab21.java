package by.gsu.igi.students.RiabchenkoSvetlana.lab2;

import java.util.Scanner;

/**
 * Created by sryabchenko on 20.10.17.
 */
public class Lab21 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");

        String nextString = scanner.next();
        System.out.println(nextString);

        int nextInt = scanner.nextInt();
        System.out.println(nextInt);
    }


}
