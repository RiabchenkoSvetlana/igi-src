package by.gsu.igi.students.RiabchenkoSvetlana.lab2;

import java.util.Scanner;

public class Lab2_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Текстовые данные ");
        System.out.print("Радиус = ");
        float radius = scanner.nextFloat();
        System.out.println("Ожидаемый вывод: ");
        System.out.println("Периметр = " + 2 * Math.PI * radius);
        System.out.println("Площадь = " + Math.PI * Math.pow(radius, 2));
    }
}
