package by.gsu.igi.students.RiabchenkoSvetlana.lab2;

import java.util.Scanner;

public class Lab2_2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первое число: ");
        int firstNumeric = scanner.nextInt();
        System.out.print("Введите второе число: ");
        int secondNumeric = scanner.nextInt();
        System.out.println("Ожидаемый вывод: ");
        System.out.println(firstNumeric + " + " + secondNumeric + " = " + (firstNumeric + secondNumeric));
        System.out.println(firstNumeric + " - " + secondNumeric + " = " + (firstNumeric - secondNumeric));
        System.out.println(firstNumeric + " * " + secondNumeric + " = " + (firstNumeric * secondNumeric));
        System.out.println(firstNumeric + " / " + secondNumeric + " = " + (firstNumeric / secondNumeric));
        System.out.println(firstNumeric + " mod " + secondNumeric + " = " + (firstNumeric / secondNumeric));

    }
}